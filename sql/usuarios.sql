CREATE TABLE usuarios (
	id int NOT NULL AUTO_INCREMENT,
	nombre text NOT NULL,
	nick text NOT NULL,
	email varchar(100) NOT NULL,
	documento int NOT NULL,
	tipo text NOT NULL,
	clave text NOT NULL,
	activo tinyint(2) DEFAULT 1,
	fechaCreado datetime DEFAULT current_timestamp,
	fechaEditado datetime DEFAULT current_timestamp,
	PRIMARY KEY (id),
	UNIQUE(documento),
	UNIQUE(email)
);

--
-- CONSULTA DE PRUEBA
--
	-- INSERT INTO usuarios 
	-- (nombre, nick, email, documento, tipo, clave, fechaCreado, fechaEditado)
	-- VALUES
	-- ('Alex Casadevall', 'lexcasa', 'lexcasa@gmail.com', '51008361', 'admin', MD5('alex123'), now(), now());