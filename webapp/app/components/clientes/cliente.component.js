const ClienteComp = Vue.component('cliente-component', function (callback) {
	let viewUrl = './app/components/clientes/clienteFormView.html'

	axios.get(viewUrl).then( function (result){
		let view = result.data;

		callback({
			data: function () {
				return {
					cliente: {
						activo: 1
					}
				}
			},
			methods: {
				guardar: function () {
					let cliente = copy(this.cliente);
					let token = localStorage.getItem('token-app-' + APP_NAME);

					const headers = {
						token: token
					}

					if(cliente.activo == 1 || cliente.activo == true){
						cliente.activo = 1;
					} else {
						cliente.activo = 0;
					}

					if(cliente.id){
						axios.put(API + 'cliente/edit', cliente, {headers: headers}).then( (res) => {
							if(!res.data.error){
								router.push('/clientes');
							} else {
								kickOut()
							}
						})
					} else {
						axios.post(API + 'cliente/new', cliente, {headers: headers}).then( (res) => {
							if(!res.data.error){
								router.push('/clientes');
							}
						})
					}
				},
				eliminar: function () {
					let cliente = copy(this.cliente);
					if(confirm("¿Esta seguro que quiere eliminar este cliente?")){
						axios.delete(API + 'cliente/delete/' + cliente.id).then( (res) => {
							if(!res.error){
								router.push('/clientes');
							}
						})
					}
				}
			},
			mounted: function (){
				let id    = app._route.params.id ? app._route.params.id : undefined
				let token = localStorage.getItem('token-app-' + APP_NAME);

				// Verifico el token
				verifyToken(token)

				const headers = {
					token: token
				}

				if(id){
					axios.get(API + 'cliente/' + id, {headers: headers}).then( (res) => {

						if(!res.data.error){
							let cliente  = res.data.response;
							this.cliente = cliente;
						} else {
							// Fuerzo un logout del usuario
							kickOut()
						}
					})
				}

				console.log("My Token :: ", token);
			},
			template: view
		})
	})
})