const DashboardComp = Vue.component('dashboard-component', function (callback) {
	let viewUrl = './app/components/dashboard/dashboardView.html'

	axios.get(viewUrl).then( function (result){
		let view = result.data;

		callback({
			data: function () {
				return {
					clientes: []
				}
			},
			methods: {
				
			},
			mounted: function (){},
			template: view
		})
	})
})