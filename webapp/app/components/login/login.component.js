const LoginComp = Vue.component('login-component', function (callback) {
	let viewUrl = './app/components/login/loginView.html'

	axios.get(viewUrl).then( function (result){
		let view = result.data;

		callback({
			data: function () {
				return {
					usr: {},
					error: ''
				}
			},
			methods: {
				loginUser: function () {
					const user = copy(this.usr);
					axios.post(API + 'login', user).then( (res) => {
						let rs = res.data;

						if(!rs.error){
							// Guardar en el localStorage
							// Token del response
							localStorage.setItem('token-app-' + APP_NAME, rs.response.token)
							router.push('/dashboard');
						} else {
							this.error = rs.error;
						}

					}, (err) => {
						console.log("error ::: ", err)
					})
				}
			},
			mounted: function (){

			},
			template: view
		})
	})
})