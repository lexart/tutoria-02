Vue.component('menu-component', function (callback) {
	let viewUrl = './app/shared/menuView.html'

	axios.get(viewUrl).then( function (result){
		let view = result.data;

		callback({
			data: function () {
				return {}
			},
			methods: {
				
			},
			mounted: function (){
				let token = localStorage.getItem('token-app-' + APP_NAME);

				// Verifico el token
				verifyToken(token)
			},
			template: view
		})
	})
})