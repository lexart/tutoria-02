let copy = function (obj){
	return JSON.parse( JSON.stringify(obj) )
}

const verifyToken = function (token) {
	if(token == undefined || token == ''){
		window.location.href = '/'
		window.localStorage.clear()
		return false;
	}
}

const kickOut = function () {
	window.location.href = '/'
	window.localStorage.clear()
}

// Definir los componentes de forma dinámica
const Login   	= LoginComp;
const Clientes 	= ClientesComp;
const Cliente 	= ClienteComp;
const Dashboard = DashboardComp;

const routes = [
  { path: '/', component: Login },
  { path: '/dashboard', component: Dashboard },
  { path: '/clientes', component: Clientes },
  { path: '/cliente/edit/:id', component: Cliente },
  { path: '/cliente/new', component: Cliente }
]

const router = new VueRouter({
  routes 
})

let app = new Vue({
	router,
	data: {
		title: 'Mi blog',
		state: 'listado',
		articulo: undefined
	},
	methods: {
		changeState: function (state, obj){
			this.state = state;
			
			if(obj){
				this.articulo = obj;
			}
		}
	}
}).$mount('#app')