let MensajeBienvenida = {
	obtener: function (ind, callback) {
		let mensajes = []
		// Instanciar en c/llamada del servicio
		// No se puede instanciar de forma global
		let xhttp 	 = new XMLHttpRequest(); // Hilo xhr


		xhttp.onreadystatechange = function () {
			if(this.readyState == 4 && this.status == 200){
				mensajes 		= JSON.parse(this.responseText)
				let msjSelected = mensajes[ind].mensaje; // mensajes[2].mensaje ~ "Buenas tardes"

				// La función que devuelve el mensaje seleccionado
				callback(msjSelected)
			}
		}

		xhttp.open('GET','./mensajes.json')
		xhttp.send()
	},
	obtenerRandom: function (callback){
		let mensajes = []
		// Instanciar en c/llamada del servicio
		// No se puede instanciar de forma global
		let xhttp 	 = new XMLHttpRequest(); // Hilo xhr


		xhttp.onreadystatechange = function () {
			if(this.readyState == 4 && this.status == 200){
				mensajes 		= JSON.parse(this.responseText)
				let ind 		= Math.floor(Math.random() * mensajes.length)
				let msjSelected = mensajes[ind].mensaje; // mensajes[2].mensaje ~ "Buenas tardes"

				// La función que devuelve el mensaje seleccionado
				callback(msjSelected)
			}
		}

		xhttp.open('GET','./mensajes.json')
		xhttp.send()

	}
}