let xhr = {
	getView: function (url, callback){
		let xhttp = new XMLHttpRequest()
		let response = `
			<p>(404) View not found</p>
		`
		
		xhttp.onreadystatechange = function () {
			if(this.readyState == 4 && this.status == 200){
				callback(this.responseText)
			} else if(this.readyState == 4 && this.status == 404){
				callback(response)
			}
		}
		xhttp.open('GET', url)
		xhttp.send()
	},
	get: function (url, callback){
		let xhttp = new XMLHttpRequest()
		let response = {
			error: "Error al obtener datos"
		}
		
		xhttp.onreadystatechange = function () {
			if(this.readyState == 4 && this.status == 200){
				try {
					response = JSON.parse(this.responseText)
				} catch (e){}

				callback(response)
			}
		}
		xhttp.open('GET', url)
		xhttp.send()
	},
	post: function (url, obj, callback) {
		let xhttp = new XMLHttpRequest()
		let response = {
			error: "Error al obtener datos"
		}
		
		xhttp.onreadystatechange = function () {
			if(this.readyState == 4 && this.status == 201){
				try {
					response = JSON.parse(this.responseText)
				} catch (e){}

				callback(response)
			}
		}
		xhttp.open('POST', url)
		xhttp.setRequestHeader("Content-type", "application/json");
		xhttp.send(JSON.stringify(obj))
	},
	put: function (url, obj, callback) {
		let xhttp = new XMLHttpRequest()
		let response = {
			error: "Error al obtener datos"
		}
		
		xhttp.onreadystatechange = function () {
			if(this.readyState == 4 && this.status == 200){
				try {
					response = JSON.parse(this.responseText)
				} catch (e){}

				callback(response)
			}
		}
		xhttp.open('PUT', url)
		xhttp.setRequestHeader("Content-type", "application/json");
		xhttp.send(JSON.stringify(obj))
	},
	delete: function (url, callback) {}
}