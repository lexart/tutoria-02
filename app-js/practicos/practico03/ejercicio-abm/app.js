// Ingresar a la web: https://crudcrud.com/
// Para obtener un Enpoint nuevo

const API 			   = 'https://crudcrud.com/api/d17639c5a29040b4a39f2de8d800e1dc'
let ABMCliente 		   = document.querySelectorAll('#ABM_Cliente');
ABMCliente 			   = ABMCliente[0]

let hideShowItem 	  	   = function (idMostrar, idOcultar) {
	document.querySelectorAll(idMostrar)[0].style.display = 'block'
	document.querySelectorAll(idOcultar)[0].style.display = 'none'
}

// Instanciamos la vista: 
Cliente.loadView( function (view) {
	// Cargar la view de ABM Cliente
	ABMCliente.innerHTML   = view;

	let contListadoCliente = document.querySelectorAll('#listadoCliente tbody');
	contListadoCliente 	   = contListadoCliente[0]

	// Inicializar el servicio de "obtenerCliente"
	// En tiempo de ejecución
	Cliente.obtenerTodos( function (html) {
		contListadoCliente.innerHTML = html;
	})
})
