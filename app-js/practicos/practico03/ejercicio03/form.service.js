let Form = {
	build: function (id, callback) {
		let schema = 'form.schema.json'
		let tpl    = ``

		// Mapa de métodos
		let methods = {
			guardarPersona: `
				onclick="Persona.guardar()"
			`,
			otraCosa: `
				onclick="Persona.otraCosa()"
			`
		}

		// Get del form
		xhr.get(schema, function (fields) {
			
			for (let i = 0; i < fields.length; i++) {
				let field  = fields[i]

				if(field.type != 'button'){
					tpl += `
						<div>
							<label>${field.label}</label>
							<br>
							<input type="${field.type}" name="${field.name}">
							<br>
						</div>
					`
				} else {

					let method = ``

					try {
						method = methods[field.action] // methods['guardarPersona']
					} catch (e){}

					tpl += `
						<button 
							${method}
							type="${field.type}">${field.label}</button>
					`
				}
			}

			let html = document.querySelectorAll(id)
			html 	 = html[0]

			html.innerHTML = tpl;

			// Service chain
			callback()
		})
	}
}