let xhr = {
	get: function (url, callback){
		let xhttp = new XMLHttpRequest()
		let response = {
			error: "Error al obtener datos"
		}
		
		xhttp.onreadystatechange = function () {
			if(this.readyState == 4 && this.status == 200){
				try {
					response = JSON.parse(this.responseText)
				} catch (e){}

				callback(response)
			}
		}
		xhttp.open('GET', url)
		xhttp.send()
	},
	post: function (url, obj, callback) {},
	put: function (url, obj, callback) {},
	delete: function (url, callback) {}
}