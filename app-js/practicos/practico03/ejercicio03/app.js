// Instanciar el servicio para crear formulario
Form.build('#registroPersona', function () {

	// Manipular el formulario (campos)
	let inputs = document.querySelectorAll('#registroPersona input');
	
	// Chain de servicio
	Persona.obtenerUno( function (persona) {
		console.log("Service - persona ::", persona)

		// Recorrer todos los inputs
		inputs.forEach( function (item) {
			// A c/input asignarle el valor [item.name] del objeto respuesta
			item.value = persona[item.name] // 

			if(item.type == 'checkbox'){
				item.checked = persona[item.name];
			}

			console.log("item: ", item.name)
		})
	})
})

