let Render = {
	build: function (obj, tagIn, tagOut, callback) {
		let renderTagOut = function (tagOut, html) {
			return `
				<${tagOut}>${html}</${tagOut}>
			`
		}

		let renderTagIn  = function (tagIn, obj) {
			let html = ``
			for (key in obj){
				html += `
					<${tagIn}>${obj[key]}</${tagIn}>
				`
			}
			return html;
		}

		let tagInObj = {
			tr: function (obj){
				let tds  = renderTagIn('td', obj);
				let html = renderTagOut('tr', tds)
				return html;
			}
		}

		let htmlIn 	 = ``

		// Span estructura de entrada
		try	{
			htmlIn 	 = tagInObj[tagIn](obj)
		} catch (e){
			htmlIn 	 = renderTagIn(tagIn, obj)
		}

		let htmlOut  = renderTagOut(tagOut, htmlIn)

		// Inserto el DIV dentro del div resultado
		callback(htmlOut);
	}
}	