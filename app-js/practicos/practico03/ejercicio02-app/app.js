let div = document.querySelectorAll('div')
div 	   = div[0]

// Llamo el servicio de Productos
Producto.obtenerTodos( function (productos) {
	console.log("productos ::", productos)

	let tpl = ``

	for (let i = productos.length - 1; i >= 0; i--) {
		let producto = productos[i]

		Render.build(producto, 'li', 'ul', function (html) {
			tpl += html;
		})
	}

	// Luego de resolver el template
	// Agrego en nuestro ul
	div.innerHTML = tpl;

	console.log("template Out: ", tpl)
})