let Producto = {
	obtenerTodos: function (callback){
		let xhttp = new XMLHttpRequest()
		xhttp.onreadystatechange = function (){
			if(this.readyState == 4 && this.status == 200){
				let productos = JSON.parse(this.responseText);

				// Envio produtos en el callback
				callback(productos)
			}
		}
		xhttp.open('GET', 'productos.json')
		xhttp.send()
	}
}