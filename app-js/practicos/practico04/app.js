let appCont = document.querySelectorAll('#app');
let navCont = document.querySelectorAll('#nav');

appCont 	= appCont[0];
navCont	 	= navCont[0];

let navItems = `
	<li>
		<a href="#clientes" onclick="loadView('clientes')">Clientes</a>
	</li>
	<li>
		<a href="#productos" onclick="loadView('productos')">Productos</a>
	</li>
	<li>
		<a href="#" onclick="logout()">Cerrar sesión</a>
	</li>
`

let hideShowItem 	  	   = function (idMostrar, idOcultar) {
	document.querySelectorAll(idMostrar)[0].style.display = 'block'
	document.querySelectorAll(idOcultar)[0].style.display = 'none'
}

let logout   = function (){
	window.localStorage.setItem('token-sistema-abm','')
	window.location.hash = ''
	window.location.reload()
}

let loadView = function (component){
	console.log("component elegido :: ", component)
	let components = {
		'login': function (){
			Login.build( function (formView){
				appCont.innerHTML = formView;

			})
		},
		'clientes': function (){
			Cliente.build( function (formView){
				appCont.innerHTML = formView;

				let contListadoCliente = document.querySelectorAll('#listadoCliente tbody');
				contListadoCliente 	   = contListadoCliente[0]
				window.location.hash   = '#'+component

				// Inicializar el servicio de "obtenerCliente"
				// En tiempo de ejecución
				Cliente.obtenerTodos( function (html) {
					contListadoCliente.innerHTML = html;
					navCont.innerHTML = navItems;
					
				})
			})
		},
		'productos': function (){
			Producto.build( function (formView){
				appCont.innerHTML = formView;

				let contListadoProducto = document.querySelectorAll('#listadoProducto tbody');
				contListadoProducto 	= contListadoProducto[0]
				window.location.hash    = '#'+component

				// Inicializar el servicio de "obtenerCliente"
				// En tiempo de ejecución
				Producto.obtenerTodos( function (html) {
					contListadoProducto.innerHTML = html;
					navCont.innerHTML = navItems;
				})
			})
		}
	}

	try {
		components[component]()
	} catch (e){
		appCont.innerHTML = `
			<p>(404) Componente no encontrado.</p>
		`;
		navCont.innerHTML = navItems;
		window.location.hash = '#'+component
	}
}

// Verifico el token

let tokenStorage = window.localStorage.getItem('token-sistema-abm')

if(tokenStorage){
	Auth.verify(tokenStorage, function (auth){
		console.log("que verifico: ", auth)
	})
} else {
	Login.build( function (formView){
		appCont.innerHTML = formView;
	})
}

