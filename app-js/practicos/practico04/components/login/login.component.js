let Login = {
	// Se dedica a la vista
	build: function (callback){
		let view = './components/login/login.view.html';
		xhr.getView(view, function (html){
			callback(html)
		})
	},
	auth: function (idCont){
		let inputs 	= document.querySelectorAll(idCont+' input');
		let usuario = {}

		inputs.forEach( function (item){
			usuario[item.name] = item.value;
		})

		// Auth de usuario
		xhr.get('./auth.json', function (res){
			if(usuario.usuario == res.user && usuario.clave == res.password){
				let token = res.token;
				// Guardar el token en memoria para luego persistir
				// Guardo en el localStorage
				window.localStorage.setItem('token-sistema-abm', token);

				console.log("ok usuario ::")

				// Llamo la vista de cliente o el componente
				loadView('clientes')

			} else {
				alert("Usuario y/o clave incorrecto.")
			}
		})
	}
}