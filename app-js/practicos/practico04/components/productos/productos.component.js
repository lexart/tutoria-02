let Producto = {
	build: function (callback) {
		let view = './components/productos/productos.view.html'

		xhr.getView(view, function (html) {
			callback(html)
		})
	},
	obtenerTodos: function (callback) {
		// Ejemplo del schema
		let productos = []

		xhr.get(API + '/productos', function (resProductos) {
			// Esto me viene del CrudCrud
			productos = resProductos;

			let tpl = ``

			for (let i = 0; i <= productos.length - 1; i++) {
				let producto = productos[i]
				let activo  = producto.activo ? 'SI' : 'NO' // IF en linea
				
				tpl += `
					<tr>
						<td>${i+1}</td>
						<td>${producto.nombre}</td>
						<td>${producto.precio}</td>
						<td>${activo}</td>
						<td>
							<button onclick="Producto.obtenerPorId('${producto._id}')">Editar</button>
							<button>Eliminar</button>
						</td>
					</tr>
				`
			}

			callback(tpl)
		})
	},
	obtenerPorId: function (id, callback) {
		xhr.get(API + '/productos/' + id, function (producto) {
			console.log("producto por ID: ", producto)

			let fields 	= document.querySelectorAll('#formProducto .field');

			fields.forEach( function (item) {
				if(item.type == 'checkbox'){
					item.checked = producto[item.name];
				} else {
					item.value = producto[item.name];
				}
			})

			// Mostrar formulario
			hideShowItem('#formProducto','#listadoProducto')
		})
	},
	guardar: function (callback) {
		// Campos del formulario
		let fields 	= document.querySelectorAll('#formProducto .field');
		let producto = {}
		let isEdit 	= false;
		let id;

		fields.forEach( function (item) {

			// Chequeo que el campo hidden de nombre _id
			if(item.name == '_id' && item.value != ''){
				isEdit = true;
				id 	   = item.value
			}

			if(item.name != '_id'){
				if(item.type == 'checkbox'){
					producto[item.name] = item.checked;
				} else {
					producto[item.name] = item.value;
				}
			}
		})

		// Para crear el usuario
		if(!isEdit){
			xhr.post(API + '/productos', producto, function (res) {
				console.log("res API ::", res)
				alert("Producto creado correctamente")

				// Recargar la pagina
				window.location.reload()
			})
		} else {
			xhr.put(API + '/productos/' + id, producto, function (res) {
				console.log("res API ::", res)
				alert("Producto actualizado correctamente")

				// Recargar la pagina
				window.location.reload()
			})
		}
	}
}