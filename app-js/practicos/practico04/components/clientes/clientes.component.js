let Cliente = {
	build: function (callback) {
		let view = './components/clientes/clientes.view.html'

		xhr.getView(view, function (html) {
			callback(html)
		})
	},
	obtenerTodos: function (callback) {
		// Ejemplo del schema
		let clientes = []

		xhr.get(API + '/clientes', function (resClientes) {
			// Esto me viene del CrudCrud
			clientes = resClientes;

			let tpl = ``

			for (let i = 0; i <= clientes.length - 1; i++) {
				let cliente = clientes[i]
				let activo  = cliente.activo ? 'SI' : 'NO' // IF en linea
				
				tpl += `
					<tr>
						<td>${i+1}</td>
						<td>${cliente.nombre}</td>
						<td>${cliente.tipo}</td>
						<td>${cliente.documento}</td>
						<td>${cliente.rut}</td>
						<td>${activo}</td>
						<td>
							<button onclick="Cliente.obtenerPorId('${cliente._id}')">Editar</button>
							<button>Eliminar</button>
						</td>
					</tr>
				`
			}

			callback(tpl)
		})
	},
	obtenerPorId: function (id, callback) {
		xhr.get(API + '/clientes/' + id, function (cliente) {
			console.log("cliente por ID: ", cliente)

			let fields 	= document.querySelectorAll('#formCliente .field');

			fields.forEach( function (item) {
				if(item.type == 'checkbox'){
					item.checked = cliente[item.name];
				} else {
					item.value = cliente[item.name];
				}
			})

			// Mostrar formulario
			hideShowItem('#formCliente','#listadoCliente')
		})
	},
	guardar: function (callback) {
		// Campos del formulario
		let fields 	= document.querySelectorAll('#formCliente .field');
		let cliente = {}
		let isEdit 	= false;
		let id;

		fields.forEach( function (item) {

			// Chequeo que el campo hidden de nombre _id
			if(item.name == '_id' && item.value != ''){
				isEdit = true;
				id 	   = item.value
			}

			if(item.name != '_id'){
				if(item.type == 'checkbox'){
					cliente[item.name] = item.checked;
				} else {
					cliente[item.name] = item.value;
				}
			}
		})

		// Para crear el usuario
		if(!isEdit){
			xhr.post(API + '/clientes', cliente, function (res) {
				console.log("res API ::", res)
				alert("Cliente creado correctamente")

				// Recargar la pagina
				window.location.reload()
			})
		} else {
			xhr.put(API + '/clientes/' + id, cliente, function (res) {
				console.log("res API ::", res)
				alert("Cliente actualizado correctamente")

				// Recargar la pagina
				window.location.reload()
			})
		}
	}
}