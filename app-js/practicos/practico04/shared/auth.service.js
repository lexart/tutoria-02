let Auth = {
	verify: function (token, callback){
		xhr.get('./auth.json', function (res){
			if(res.token == token){
				let hash 		= window.location.hash;
				let stateComp 	= hash.replace('#','')

				loadView(stateComp)
				callback({auth: true})
			} else {
				// Vacio la memoria
				window.localStorage.clear()
				loadView('login')
				callback({auth: false})
			}
		})
	}
}