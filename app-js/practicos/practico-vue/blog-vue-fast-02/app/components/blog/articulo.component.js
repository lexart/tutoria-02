// t=2
Vue.component('blog-articulo-form', function (callback) {
	let viewUrl = './app/components/blog/articuloFormView.html'

	// Instanciando el axios que hace un GET a la url del template
	// Esto resuelve un documento HTML

	// t=2 + i
	axios.get(viewUrl).then( function (result){
		let view = result.data;

		callback({
			data: function () {
				return {
					articulo: {
						titulo: '',
						autor: '',
						desc: '',
						link: '',
						img: ''
					}
				}
			},
			methods: {
				guardarArticulo: function (){
					let articulo = copy(this.articulo);

					if(articulo._id){
						// Edita
						let id = this.articulo._id;
						delete articulo._id;

						axios.put(API + '/articulos/' + id, articulo).then( (res) => {
							console.log("res ::", res.data)

							// Instancia de vue
							app.changeState('listado')
						})
					} else {
						// Crea
						axios.post(API + '/articulos', articulo).then( (res) => {
							console.log("res ::", res.data)

							// Instancia de vue
							app.changeState('listado')
						})
					}
				}
			},
			mounted: function (){
				if(app._data.articulo){
					let articulo = app._data.articulo;
					this.articulo = articulo;
				}
			},
			template: view
		})
	})
})