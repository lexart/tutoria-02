// t=2
Vue.component('blog-articulos', function (callback) {
	let viewUrl = './app/components/blog/articulosView.html'

	// Instanciando el axios que hace un GET a la url del template
	// Esto resuelve un documento HTML
	// t=2 + i
	axios.get(viewUrl).then( function (result){
		let view = result.data;

		// t=4 + i + k (+~ ∂)
		callback({
			data: function () {
				return {
					articulos: []
				}
			},
			methods: {
				obtenerArticulo: function (articulo){
					console.log("articulo ::", articulo)

					app.changeState('formulario', articulo)
				}
			},
			// t=3 + i
			mounted: function (){
				console.log("blog-articulos ::")
				// t=4 + i
				axios.get(API + '/articulos').then( (res) => {
					let articulos = res.data;
					// t=4 + i + k
					this.articulos = articulos;
				})
			},
			template: view
		})
	})
})