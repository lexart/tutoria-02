let copy = function (obj){
	return JSON.parse( JSON.stringify(obj) )
}
let app = new Vue({
	el: '#app',
	data: {
		title: 'Mi blog',
		state: 'listado',
		articulo: undefined
	},
	methods: {
		changeState: function (state, obj){
			this.state = state;
			
			if(obj){
				this.articulo = obj;
			}
		}
	}
})