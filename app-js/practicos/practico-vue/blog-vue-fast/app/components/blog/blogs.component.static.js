// Instanciar componente de blogs
// Forma estática
Vue.component('blog-listado', function (resolve){
	axios.get('./app/components/blog/blogsView.html').then( function (res){
		let view = res.data;

		// Resuelva el componente async
		resolve({
			template: view,
			data: function () {
				return {
					titulo: "Blogs de la semana",
					blogItems: [
						{
							id: 1,
							titulo: 'Blog #1',
							desc: `
								Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
								tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
								quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
								consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
								cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
								proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
							`,
							img: 'https://concepto.de/wp-content/uploads/2015/03/paisaje-e1549600034372.jpg'
						},
						{
							id: 2,
							titulo: 'Blog #2 - Otro paisaje',
							desc: `
								Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
								tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
								quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo.
							`,
							img: 'https://www.dzoom.org.es/wp-content/uploads/2017/07/seebensee-2384369-810x540.jpg'
						}

					]
				}
			}
		})
	})
})