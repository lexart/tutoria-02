Vue.component('blog-form', function (resolve){
	axios.get('./app/components/blog/blogFormView.html').then( function (res){
		let view = res.data;

		// Resuelva el componente async
		resolve({
			template: view,
			data: function () {
				return {
					titulo: "Crear/Editar blog",
					blogItem: {
						title: '',
						desc: '',
						author: '',
						img: '',
						link: ''
					}
				}
			},
			methods: {
				guardarBlog: function(){
					let blogItem = copy(this.blogItem);

					blogItem.fechaCrea = new Date()

					if(blogItem._id){
						let id = this.blogItem._id;

						delete blogItem._id;

						axios.put(API + 'blogs/' + id, blogItem).then( (res) => {
							console.log("respuesta crud: ", res, app.state)

							app.changeState('listado')
						})

					} else {
						axios.post(API + 'blogs', blogItem).then( (res) => {
							console.log("respuesta crud: ", res, app.state)

							app.changeState('listado')
						})
					}

					console.log("blogItem:: ", blogItem)
				}
			},
			mounted: function (){
				// Verificar e vue object "app"
				// Chequeo el ._data ~ data: {}
				if(app._data.blogItem){
					this.blogItem = app._data.blogItem;
					console.log("componente blog ready ::")
				}
			}
		})
	})
})