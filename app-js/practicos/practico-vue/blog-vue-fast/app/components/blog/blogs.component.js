// Instanciar componente de blogs
// Forma estática
Vue.component('blog-listado', function (resolve){
	axios.get('./app/components/blog/blogsView.html').then( function (res){
		let view = res.data;

		// Resuelva el componente async
		resolve({
			template: view,
			data: function () {
				return {
					titulo: "Blogs de la semana",
					blogItems: []
				}
			},
			methods: {
				obtenerBlogItem: function (blogItem){
					console.log("blogItem ::", blogItem)

					app.changeState('formulario', blogItem)
				}
			},
			mounted: function (){
				// Obtener información de la API con Axios:

				axios.get(API + 'blogs').then( (res) => {
					console.log("blog result ::", res.data)
					
					// Lo tenemos disponible como un array de objeto
					let blogResult = res.data;

					// 
					this.blogItems = blogResult;
				})

				console.log("componente blog ready ::")
			}
		})
	})
})