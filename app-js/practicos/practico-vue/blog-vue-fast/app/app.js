const API_BLOG = 'https://blog.lexart.tech/wp-json/wp/v2/posts';
let copy = function (obj){
	return JSON.parse( JSON.stringify(obj) );
}
let app = new Vue({
	el: '#app',
	data: {
		title: 'Bienvenidos a mi Blog',
		state: 'listado',
		blogItem: undefined
	},
	methods:{
		changeState: function (state, obj){
			this.state    = state;
			this.blogItem = undefined;

			if(obj){
				this.blogItem = obj;
			}
		}
	}
})