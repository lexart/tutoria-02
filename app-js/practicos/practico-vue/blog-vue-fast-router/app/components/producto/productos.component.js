// t=2
let ProductosComp = Vue.component('productos', function (callback) {
	let viewUrl = './app/components/producto/productosView.html'

	// Instanciando el axios que hace un GET a la url del template
	// Esto resuelve un documento HTML
	// t=2 + i
	axios.get(viewUrl).then( function (result){
		let view = result.data;

		// t=4 + i + k (+~ ∂)
		callback({
			data: function () {
				return {
					productos: []
				}
			},
			methods: {

			},
			// t=3 + i
			mounted: function (){

				// t=4 + i
				axios.get(API + '/productos').then( (res) => {
					let productos = res.data;
					// t=4 + i + k
					this.productos = productos;
				})
			},
			template: view
		})
	})
})