// t=2
let ProductoForm = Vue.component('producto-form', function (callback) {
	let viewUrl = './app/components/producto/productoFormView.html'

	// Instanciando el axios que hace un GET a la url del template
	// Esto resuelve un documento HTML

	// t=2 + i
	axios.get(viewUrl).then( function (result){
		let view = result.data;

		callback({
			data: function () {
				return {
					producto: {
						nombre: '',
						precio: 0,
						cantidad: 1
					},
					// Esto viene de la instancia de "let app = new Vue"
					params: app._route.params
				}
			},
			methods: {
				guardarProducto: function (){
					let producto = copy(this.producto);

					if(producto._id){
						// Edita
						let id = this.producto._id;
						delete producto._id;

						axios.put(API + '/productos/' + id, producto).then( (res) => {
							console.log("res ::", res.data)

							// Instancia de vue
							router.push('/productos')
						})
					} else {
						// Crea
						axios.post(API + '/productos', producto).then( (res) => {
							console.log("res ::", res.data, router)

							// Instancia de vue
							router.push('/productos')
						})
					}
				}
			},
			mounted: function (){
				// Obtener la ID de la URL
				let id = this.params.id;

				console.log("router this ", this.params)

				if(id){
					axios.get(API + '/productos/' + id).then( (res) => {
						this.producto = res.data;
					})
				}
			},
			template: view
		})
	})
})