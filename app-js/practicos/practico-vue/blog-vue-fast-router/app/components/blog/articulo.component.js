// t=2
let BlogArticuloForm = Vue.component('blog-articulo-form', function (callback) {
	let viewUrl = './app/components/blog/articuloFormView.html'

	// Instanciando el axios que hace un GET a la url del template
	// Esto resuelve un documento HTML

	// t=2 + i
	axios.get(viewUrl).then( function (result){
		let view = result.data;

		callback({
			data: function () {
				return {
					articulo: {
						titulo: '',
						autor: '',
						desc: '',
						link: '',
						img: ''
					},
					// Esto viene de la instancia de "let app = new Vue"
					params: app._route.params
				}
			},
			methods: {
				guardarArticulo: function (){
					let articulo = copy(this.articulo);

					if(articulo._id){
						// Edita
						let id = this.articulo._id;
						delete articulo._id;

						axios.put(API + '/articulos/' + id, articulo).then( (res) => {
							console.log("res ::", res.data)

							// Instancia de vue
							router.push('/')
						})
					} else {
						// Crea
						axios.post(API + '/articulos', articulo).then( (res) => {
							console.log("res ::", res.data, router)

							// Instancia de vue
							router.push('/')
						})
					}
				}
			},
			mounted: function (){
				// Obtener la ID de la URL
				let id = this.params.id;

				console.log("router this ", this.params)

				if(id){
					axios.get(API + '/articulos/' + id).then( (res) => {
						this.articulo = res.data;
					})
				}
			},
			template: view
		})
	})
})