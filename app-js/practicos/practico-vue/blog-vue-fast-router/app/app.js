let copy = function (obj){
	return JSON.parse( JSON.stringify(obj) )
}

// Definir los componentes de forma dinámica
const Articulos   = BlogArticulos;
const Articulo 	  = BlogArticuloForm;
const Productos   = ProductosComp;
const Producto 	  = ProductoForm;

const routes = [
  { path: '/', component: Articulos },
  { path: '/articulo/new', component: Articulo },
  { path: '/articulo/edit/:id', component: Articulo },
  { path: '/productos', component: Productos },
  { path: '/producto/new', component: Producto },
  { path: '/producto/edit/:id', component: Producto }
]

const router = new VueRouter({
  routes 
})

let app = new Vue({
	router,
	data: {
		title: 'Mi blog',
		state: 'listado',
		articulo: undefined
	},
	methods: {
		changeState: function (state, obj){
			this.state = state;
			
			if(obj){
				this.articulo = obj;
			}
		}
	}
}).$mount('#app')