const express 	 = require('express');
const bodyParser = require('body-parser');
const cors 		 = require('cors');
const app 	  	 = express();

// Requiero de manera global la conexión con la base de datos
global.conn 	 = require('./config/conn');
const Cliente 	 = require('./services/clientes.service')
const Usuario 	 = require('./services/usuarios.service')
const Mdl 		 = require('./services/middleware.service')

const port 	  	 = 3000;

app.use(cors())
app.use(bodyParser.json())

app.get('/', function (req, res) {
	let response = {hola: "Mundo"}

	res.set(['Content-Type', 'application/json']);
    res.send(response);
})

app.post('/login', async function (req, res) {
	let post = req.body;
	let response = await Usuario.login(post.user, post.password);

	res.set(['Content-Type', 'application/json']);
    res.send(response);
})

app.get('/clientes/all/:offset?/:limit?', Mdl.middleware, async function (req, res) {
	let offset   = req.params.offset;
	let limit 	 = req.params.limit;

	let response = await Cliente.todos(offset, limit);

	res.set(['Content-Type', 'application/json']);
    res.send({response: response});
})

app.get('/cliente/:id', Mdl.middleware, async function (req, res) {
	let id   = req.params.id;

	let response = await Cliente.uno(id);

	res.set(['Content-Type', 'application/json']);
    res.send({response: response});
})

app.post('/cliente/new', Mdl.middleware, async function (req, res) {
	let post   = req.body;

	let response = await Cliente.nuevo(post);

	res.set(['Content-Type', 'application/json']);
    res.send(response);
})

app.put('/cliente/edit', Mdl.middleware, async function (req, res) {
	let post   = req.body;

	let response = await Cliente.editar(post);

	res.set(['Content-Type', 'application/json']);
    res.send(response);
})

app.delete('/cliente/delete/:id', Mdl.middleware, async function (req, res) {
	let id   = req.params.id;

	let response = await Cliente.eliminar({id: id});

	res.set(['Content-Type', 'application/json']);
    res.send(response);
})

app.listen(port, function () {
  console.log(`SISTEMA DE ABM - Clientes :: ${port}`)
})