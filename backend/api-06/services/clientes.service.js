const tablaNombre = 'clientes';

let Clientes = {
	todos: async function (offset, limit) {
		
		offset = offset ? offset : 0;
		limit  = limit ? limit : 50;

		const sql = `
			SELECT * FROM ${tablaNombre}
			LIMIT ${offset}, ${limit}
		`
		let response = []
		
		try {
			response = await conn.query(sql);
		} catch(e){}

		return response;
	},
	uno: async function (id) {
		id = id ? id : 0;

		const sql = `
			SELECT * FROM ${tablaNombre}
			WHERE id = ${id}
		`
		let response = []
		
		try {
			response = await conn.query(sql);
		} catch(e){}

		return response.length > 0 ? response[0] : {};
	},
	nuevo: async function (cliente) {
		const sql = `
			INSERT INTO ${tablaNombre} (
					 nombre, 
					 apellido,
					 rut,
					 documento,
					 activo
			)
			VALUES (
					'${cliente.nombre}',
					'${cliente.apellido}',
					'${cliente.rut}',
					'${cliente.documento}',
					1
			);
		`
		let response = []
		
		try {
			response = await conn.query(sql);
		} catch(e){}

		return response.insertId ? {response: "Ingresado correctamente"} : {error: "Error al insertar"};
	},
	editar: async function (cliente) {
		const sql = `
			UPDATE ${tablaNombre}  SET 
					 nombre = '${cliente.nombre}', 
					 apellido = '${cliente.apellido}',
					 rut = '${cliente.rut}',
					 documento = '${cliente.documento}',
					 activo = '${cliente.activo}'
			WHERE id = ${cliente.id}
		`
		let response = []
		
		try {
			response = await conn.query(sql);
		} catch(e){}

		return response.changedRows ? {response: "Actualizado correctamente"} : {error: "Error al actualizar"};
	},
	eliminar: async function (cliente) {
		const sql = `
			DELETE FROM ${tablaNombre} WHERE id = ${cliente.id}
		`
		let response = []
		
		try {
			response = await conn.query(sql);
		} catch(e){}

		console.log("response: ", response)

		return response.affectedRows ? {response: "Eliminado correctamente"} : {error: "Error al eliminar"};
	}
}
module.exports = Clientes;