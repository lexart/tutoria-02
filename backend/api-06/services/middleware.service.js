const utils = require('./utils.service')

const Mdl = {
	middleware: async function (req, res, next){
		
		let error = {"error":"Token inválido"}

		const tablaNombre = 'usuarios'

		// Obtener los usuarios
		const sql = `
			SELECT * FROM ${tablaNombre}
		`
		let response = []
		
		try {
			response = await conn.query(sql);
		} catch(e){}

		let userCorrect = false;

		if(response.length > 0){
			response.map( (usr) => {
				let fastToken = utils.makeToken(usr.email, usr.documento, key);
				
				if(req.headers.token == fastToken){
					userCorrect = true;
					return;
				}
			})
		}

		// Si el usuario es correcto
		// Entrego la informacion
		if(userCorrect){
			next();
		} else {
			error.yourToken = req.headers.token;

			res.set(['Content-Type', 'application/json']);
			res.send(error);
		}
	}
}
module.exports = Mdl;