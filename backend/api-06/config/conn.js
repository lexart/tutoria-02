const mysql      = require('mysql');
const connection = mysql.createConnection({
  host     : 'localhost',
  user     : 'lexcasa',
  password : 'password',
  database : 'sistema_abm'
});
 
connection.connect();
global.key = 'hola.!2123.Mundo';

const Conn = {
	query: function (sql) {
		return new Promise( (resolve, reject) => {
			connection.query(sql, function (error, results, fields) {			  
			  if (error){
			  	resolve(error)
			  } else {
			  	resolve(results);
			  }

			});
		})
	}
}

module.exports = Conn;