const express 	 = require('express');
const bodyParser = require('body-parser');
const app 	  = express();
const port 	  = 3000;


app.use(bodyParser.json())

app.get('/', function (req, res) {
	let response = {hola: "Mundo"}

	res.set(['Content-Type', 'application/json']);
    res.send(response);
})

app.get('/productos', function(req, res) {

	// Llamo al módulo de Producto
	const Producto = require('./services/productos.service')

	// Obtenemos los productos del servicio
	const productos = Producto.all();

	// Enviamos como una response
	const response = productos;

	res.set(['Content-Type', 'application/json']);
	res.send(response);
});

app.post('/productos/carrito', function (req, res) {
	// Llamo al módulo de Producto
	const Producto = require('./services/productos.service')

	// Son los productos que me llegan desde el POSTMAN
	let productos = req.body;

	// La respuesta procesada de los productos
	let response  = Producto.procTotal(productos);

	res.set(['Content-Type', 'application/json']);
  	res.send(response);
})

app.get('/productos/:cod', function(req, res) {

	// Obtenemos del parametro de url como Vue
	let cod = req.params.cod;

	// Llamo al módulo de Producto
	const Producto = require('./services/productos.service')

	// Obtenemos los productos del servicio
	const productos = Producto.all();
	const producto  = Producto.getByCod(productos, cod);

	// Enviamos como una response
	const response = producto;

	res.set(['Content-Type', 'application/json']);
	res.send(response);
});


app.listen(port, function () {
  console.log(`Example app listening at http://localhost:${port}`)
})