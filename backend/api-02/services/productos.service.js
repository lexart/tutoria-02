const Producto = {
	all: function () {
		return [ 
			{
	  			nombre: "Banana",
	  			cod: "ABC123",
	  			cantidad: 1,
	  			precio:123,
	  			moneda: "\$"
	  		},
	  		{
	  			nombre: "Manzana",
	  			cod: "AAA123",
	  			cantidad: 1,
	  			precio:11,
	  			moneda: "\$"
	  		},
	  		{
	  			nombre: "Naranja",
	  			cod: "A123",
	  			cantidad: 1,
	  			precio:11,
	  			moneda: "\$"
	  		}

	  ];
	},
	getByCod: function (productos, cod) {
		let findProducto = undefined
		// Transformo el cod en mayuscula: cod.toUpperCase()
		let codProd 	 = cod ? cod.toUpperCase() : undefined

		if(codProd && productos && productos.length > 0){
			productos.map( (producto) => {
				if(producto.cod == codProd){
					findProducto = producto;
					return;
				}
			})
		}
		return findProducto ? {response: findProducto} : {error: "Producto no encontrado"};
	},
	procTotal: function (productos) {
		let total 	= 0
		let moneda 	= "N/A"

		if(productos && productos.length > 0){
			moneda = "\$"
			productos.map( (producto) => {
				total += producto.cantidad * producto.precio;
			})
		}

		let response = {
			total: total,
			moneda: moneda
		}

		return response;
	}
}
module.exports = Producto;