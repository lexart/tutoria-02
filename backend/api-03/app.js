const express 	 = require('express');
const bodyParser = require('body-parser');
const app 	  	 = express();
const port 	  	 = 3000;
const Response 	 = require('./services/response.service')
const Calc 	     = require('./services/calc.service')
const Geom 		 = require('./services/geom.service');

app.use(bodyParser.json())

app.get('/', function (req, res) {
	let response = {hola: "Mundo"}

	res.set(['Content-Type', 'application/json']);
    res.send(response);
})


app.post('/calcular', function (req, res) {
	const op	  = req.body; 
	let result; 

	try {
		// Ejecutando el servicio
		result 	  = Calc[op.operacion](op.a, op.b);

	} catch(e){}

	res.set(['Content-Type', 'application/json'])
	res.send(
		// Abstracción de los valores del result
	 	Response.res(result)
	)
})

app.post('/calcular/geometria', function (req, res) {
	const op	  = req.body; 
	let result; 

	try {
		// Ejecutando el servicio
		result 	  = Geom[op.area](op.base, op.altura);

	} catch(e){}

	let resData = Response.res(result);

	// Si no es un error
	if(!resData.error){
		resData.response.unidad = Geom.unidad;
	}

	res.set(['Content-Type', 'application/json'])
	res.send(
		// Abstracción de los valores del result
	 	resData
	)
})

app.listen(port, function () {
  console.log(`API :: ${port}`)
})