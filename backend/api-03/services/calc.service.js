const Calc = {
	suma: function (a, b) {
		return a + b;
	},
	resta: function (a, b) {
		return a - b;
	},
	multi: function (a, b) {
		return a * b;
	},
	div: function (a, b) {
		if(b !== 0){
			return a / b
		}
		return undefined;
	}
}
module.exports = Calc;