let Response = {
	res: function (result) {
		let resData = {
			error: {
				message:"Error de operación y/o cálculo."
			}
		}

		if(result){
			resData = {
				response: {
					resultado: result
				}
			}
		}
		return resData;
	}
}
module.exports = Response;