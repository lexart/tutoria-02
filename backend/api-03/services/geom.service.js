const Geom = {
	cuadrado: function (base, altura) {
		return base == altura ? base * altura : undefined;
	}, 
	triangulo: function (base, altura) {
		return (base * altura)/2;
	},
	rectangulo: function (base, altura) {
		return base * altura;
	},
	circulo: function (radio) {
		const pi = Math.PI;
		return (pi * radio**2) 
	},
	unidad: "metros cuadrados"
}
module.exports = Geom;