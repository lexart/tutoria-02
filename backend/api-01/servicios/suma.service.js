let Calculadora = {
	suma: function (x, y){
		return x + y;
	},
	resta: function (x, y) {
		return x - y;
	},
	multi: function (x, y) {
		return x * y;
	},
	div: function (x, y) {
		if(y !== 0){
			return x / y;
		}
		return 'Err';
	}
}
module.exports = Calculadora;