const a = "Texto" // Const no es modificable
let b 	= 10 // Puede cambiar de tipo a lo largo del script

console.log("a: ", a)
console.log("b: ", b)


// Si un numero es mayor que 10

if(b > 10){
	console.log("B mayor que 10")
} else {
	console.log("B menor que 10")
}

// Recorrer un array
let arr = [1, 2, 3, 4, 4]

for (let i = arr.length - 1; i >= 0; i--) {
	console.log("arr: ", arr[i])
}

// Recorrer un objeto
let obj = {
	nombre: "Alex",
	apellido: "Casadevall",
	edad: 28
}

for (let key in obj){
	console.log("key: ", key, " value:", obj[key])
}

// Recorrer los maps
let usuarios = [
	{
		nombre: "Alex",
		apellido: "Casadevall",
		edad: 28
	},
	{
		nombre: "Juan",
		apellido: "Casadevall",
		edad: 25
	}
]

// Recorrer array de objetos en paralelo
usuarios.map( (item) => {
	console.log("item:: ", item)
})

// Funciones
const suma = function (x, y) {
	return x + y;
}

let result = suma(10, 11)

console.log("result: ", result)

// Llamar el motor de la calculadora
// const calc = require('./servicios/suma.service')
const { multi, div } = require('./servicios/suma.service')

console.log("Calc result: ", multi(10, 100), div(11,11))


