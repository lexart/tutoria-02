const spl = " ";

const botResp = {
	'hola': 'bien, en que te puedo ayudar?',
	'cómo': 'Las empanadas se hacen como quieras',
}

let Bot = {
	say: function (intent) {
		let terms = intent.split(spl);
		let respuesta;

		for (let i = terms.length - 1; i >= 0; i--) {
			// Aca normaliza los terminos de entrada
			let term = terms[i].toLowerCase()

			respuesta = botResp[term]
			
			// Si viene alguna respuesta para de verificar los términos
			if(respuesta){
				break;
			}
		}
		return respuesta;
	}
}
module.exports = Bot;