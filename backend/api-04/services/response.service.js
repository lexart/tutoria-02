let Response = {
	res: function (result) {
		let resData = {
			error: {
				message:"No te entendí"
			}
		}

		if(result){
			resData = {
				response: {
					type: "bot",
					message: result
				}
			}
		}
		return resData;
	}
}
module.exports = Response;