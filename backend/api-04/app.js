const express 	 = require('express');
const bodyParser = require('body-parser');
const app 	  	 = express();
const port 	  	 = 3000;
const Response 	 = require('./services/response.service');
const Bot 		 = require('./services/bot.service')

app.use(bodyParser.json())

app.get('/', function (req, res) {
	let response = {hola: "Mundo"}

	res.set(['Content-Type', 'application/json']);
    res.send(response);
})

app.post('/chat-bot', function (req, res) {
	const intencion = req.body;
	let result; 

	try {
		result = Bot.say(intencion.mensaje);
	} catch (e){}

	res.set(['Content-Type', 'application/json'])
	res.send(
		Response.res(result)
	)
})

app.listen(port, function () {
  console.log(`CHAT BOT - API :: ${port}`)
})