const mysql      = require('mysql');
const connection = mysql.createConnection({
  host     : 'localhost',
  user     : 'lexcasa',
  password : 'password',
  database : 'db_blackjack'
});
 
connection.connect();

const Conn = {
	query: function (sql) {
		return new Promise( (resolve, reject) => {
			connection.query(sql, function (error, results, fields) {			  
			  if (error){
			  	resolve(error)
			  } else {
			  	resolve(results);
			  }

			});
		})
	}
}

module.exports = Conn;