const BlackJack = {
	calcCartas: async function (arr) {
		const mapCartas = {
			'A': 11,
			'1': 1,
			'2': 2,
			'3': 3,
			'4': 4,
			'5': 5,
			'6': 6,
			'7': 7,
			'8': 8,
			'9': 9,
			'10':10,
			'J':10,
			'K':10,
			'Q':10
		}
		
		let sum = 0;

		arr.map( (carta) => {
			sum += mapCartas[carta];
		})

		let response 	= {
			probabilidad: '+100',
			recomendacion: 'no pedir'
		}

		if(sum <= 21){
			const valor 	= await Prob.una(sum);

			response = {
				probabilidad: valor,
				recomendacion: 'pedir'
			}
			
			if(valor >= 69){
				response.recomendacion = 'no pedir'
			}
		}

		return response;
	}
}
module.exports = BlackJack;