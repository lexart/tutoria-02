const Prob = {
	todos: async function () {
		let sql = `
			SELECT * FROM probabilidades;
		`
		let res = await conn.query(sql);
		return res;
	},
	una: async function (valor) {
		let sql = `
			SELECT * FROM probabilidades WHERE valor = '${valor}';
		`
		let res = await conn.query(sql);

		try {
			return res[0].probabilidad ? res[0].probabilidad : 0;
		} catch (e){
			return 0;
		}
	}
}

module.exports = Prob;