let Response = {
	res: function (result) {
		let resData = {
			error: {
				message:"Error al simular."
			}
		}

		if(result){
			resData = {
				response: result
			}
		}
		return resData;
	}
}
module.exports = Response;