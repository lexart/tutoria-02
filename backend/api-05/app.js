const express 	 = require('express');
const bodyParser = require('body-parser');
const app 	  	 = express();

// Requiero de manera global la conexión con la base de datos
global.conn 	 = require('./config/conn');
global.Prob 	 = require('./services/prob.service')
const BJ 		 = require('./services/blackjack.service');
const Response   = require('./services/response.service');

const port 	  	 = 3000;

app.use(bodyParser.json())

app.get('/', function (req, res) {
	let response = {hola: "Mundo"}

	res.set(['Content-Type', 'application/json']);
    res.send(response);
})

app.get('/probabilidades/all', async function (req, res) {
	
	let response 	= await Prob.todos();

	res.set(['Content-Type', 'application/json']);
    res.send(response);
})

app.post('/black-jack/simulacion', async function (req, res) {
	let cartas 		= req.body; 
	let response

	try {
		response 	= await BJ.calcCartas(cartas.cartas);
	} catch (e){}

	res.set(['Content-Type', 'application/json']);
    res.send(
    	Response.res(response)
    );
})

app.listen(port, function () {
  console.log(`BLACK JACK - API :: ${port}`)
})


