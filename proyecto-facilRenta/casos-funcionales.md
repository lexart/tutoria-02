## FUNCIONAL

## TABLA: usuario_propiedad

- (id [PK], idUsuario [FK], idPropiedad [FK], accion, precio, moneda, duracion, activo, fechaCreado, fechaEditado)

`
	--- CASO - INICIAL
	Alex: (A_I, 1, 1, 'alquilado', 1, now(), now())
	Juan: (A_I, 2, 1, 'propietario', 1, now(), now())
	Juan: (A_I, 2, 2, 'propietario', 1, now(), now())
	---
`

- IDEA: Yo puedo alquilar una propiedad, si existe la relación idUsuario:idPropietario:'propietario' en la tabla usuario_propiedad
	- INSERT: 1:1:'alquilado'

- PROBLEMA: Obtener propiedades que no estén alquiladas
	- Obtengo todas las propiedades que no tenga la relación idPropiedad:'alquilado':activo

- PROBLEMA 2: Tomamos como fecha de inicio la `fechaCreado`, lo que nos está faltando es la duración del contrato.
	- fechaFinContrato [ ]
	- ó duración en meses [x]

- PRECIOS: Agregamos un campos nuevos `precio, moneda` que es dinámico

`
	--- CASO - FINAL
	Alex: (A_I, 1, 1, 'alquilado', 1, 12000, '$', 12, now(), now())
	Juan: (A_I, 2, 1, 'propietario', 1, 12000, '$', 12, now(), now())
	Juan: (A_I, 2, 2, 'propietario', 1, 17000, '$', 24, now(), now())
	---
`


## TABLA: usuario_tarjeta

- (id [PK], idUsuario [PK], idTarjeta [FK], activo, fechaCreado, fechaEditado)

`
	-- CASO INICIAL
	Alex: (A_I, 1, 1, 1, now(), now()) Inquilino
	Juan: (A_I, 2, 1, 1, now(), now()) Propietario
	Juan: (A_I, 2, 2, 1, now(), now()) Propietario
	---
`


