Vue.component('menu-component', function (callback) {
	let viewUrl = './app/shared/menuView.html'

	axios.get(viewUrl).then( function (result){
		let view = result.data;

		callback({
			data: function () {
				return {
					title: 'FácilRenta',
					menuItems: [
						{
							name: 'Ingresar',
							to: '/login'
						},
						{
							name: 'Registrar',
							to: '/register'
						},
						{
							name: 'Propiedades',
							to: '/propiedades'
						}
					]
				}
			},
			methods: {
				
			},
			mounted: function (){
				let token = localStorage.getItem('token-app-' + APP_NAME);

				// Verificamos que el usuario esté logueado 
				// Y agregamos el item "Admin. propiedades"

				let usuario = JSON.parse( window.localStorage.getItem('_user' + APP_NAME) )

				console.log("usuario: ", usuario)

				if(usuario && usuario.user_id){
					this.menuItems.push(
						{
							name: 'Adm. Propiedades',
							to: '/admin-propiedades'
						}
					)
				}	

				// Verifico el token
				// verifyToken(token)
			},
			template: view
		})
	})
})