const Utils = {
	copy: function (obj){
		return JSON.parse( JSON.stringify(obj) )
	}, 
	verifyToken: function (token) {
		if(token == undefined || token == ''){
			window.location.href = '/'
			window.localStorage.clear()
			return false;
		}
	},
	kickOut: function () {
		window.location.href = '/'
		window.localStorage.clear()
	},
	chunk: function (arr, len) {
	  let chunks = [],
	      i = 0,
	      n = arr.length;

	  while (i < n) {
	    chunks.push(arr.slice(i, i += len));
	  }

	  return chunks;
	}
}

// CONVERT UTIL FUNCTION TO GLOBAL
for (let key in Utils){
	window[key] = Utils[key];
}
