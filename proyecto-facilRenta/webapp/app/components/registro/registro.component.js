const RegistroComp = Vue.component('registro-component', function (callback) {
	let viewUrl = './app/components/registro/registroView.html'

	axios.get(viewUrl).then( function (result){
		let view = result.data;

		callback({
			data: function () {
				return {
					user: {},
					error: '',
					success: ''
				}
			},
			methods: {
				registro: function (){
					this.error 	 = ''
					this.success = ''

					Usuarios().registro( this.user, (res) => {
						if(!res.error){
							this.success = res;

							setTimeout( function (){
								router.push('/login')
							}, 500)

						} else {
							this.error = res.error;
						}
					})
				}
			},
			mounted: function (){

			},
			template: view
		})
	})
})