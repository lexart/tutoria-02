const AdmPropiedadesComp = Vue.component('adm-propiedades-component', function (callback) {
	let viewUrl = './app/components/admPropiedades/admPropiedadesView.html'

	axios.get(viewUrl).then( function (result){
		let view = result.data;

		callback({
			data: function () {
				return {
					propiedades: [],
					propiedad: {},
					success: '',
					error: ''
				}
			},
			methods: {
				guardarPropiedad: function (){
					let propiedad = copy(this.propiedad)
					let user = JSON.parse(window.localStorage.getItem('_user' + APP_NAME))

					// Vincular la propiedad con el usuario logueado
					propiedad.user_id = user.user_id

					Propiedades().upsert(propiedad, (res) => {
						if(!res.error){
							this.success = res;

							// Si esta todo OK, que recargue la página
							window.location.reload()
							
						} else {
							this.error = res.error;
						}
					})
				},
				byPassPropiedad: function (obj) {
					this.propiedad = obj;
				}
			},
			mounted: function (){

				// Utilizando el servicio Propiedades
				Propiedades().byUser( (res)=> {
					if(!res.error){
						this.propiedades = chunk(res, 3);

						console.log("this.propiedades: ", this.propiedades)
					} else {
						console.log("error ::", res.error)
					}
				})
			},
			template: view
		})
	})
})