const Usuarios = function (){
	const model = 'usuarios/'

	return {
		registro: function (usuario, cb){
			axios.post(API + model + 'registro', usuario).then( function (res){
				if(!res.data.error){
					// Si viene bien devuelve la respuesta sin capsula: "response"
					cb(res.data.response)
				} else {
					// Caso contrario enviamos error
					cb({error: res.data.error})
				}
			}).catch( function (error){
				cb({error: 'Error en el servidor', serverError: error})
			})
		},
		login: function (usuario, cb){
			axios.post(API + model + 'login', usuario).then( function (res){
				if(!res.data.error){
					// Si viene bien devuelve la respuesta sin capsula: "response"
					cb(res.data.response)
				} else {
					// Caso contrario enviamos error
					cb({error: res.data.error})
				}
			}).catch( function (error){
				cb({error: 'Error en el servidor', serverError: error})
			})
		}
	}
}