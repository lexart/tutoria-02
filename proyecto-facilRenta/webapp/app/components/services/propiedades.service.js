const Propiedades = function (){
	const model = 'propiedades/'

	return {
		all: function (cb){
			axios.get(API + model + 'destacadas').then( function (res){
				if(!res.data.error){
					// Si viene bien devuelve la respuesta sin capsula: "response"
					cb(res.data.response)
				} else {
					// Caso contrario enviamos error
					cb({error: res.data.error})
				}
			}).catch( function (error){
				cb({error: 'Error en el servidor', serverError: error})
			})
		},
		byUser: function (cb){
			// Tomar en cuenta que para usar esta función necesitamos que el usuario esté logueado
			let user = JSON.parse(window.localStorage.getItem('_user' + APP_NAME))

			axios.get(API + model + 'by-user/' + user.user_id).then( function (res){
				if(!res.data.error){
					// Si viene bien devuelve la respuesta sin capsula: "response"
					cb(res.data.response)
				} else {
					// Caso contrario enviamos error
					cb({error: res.data.error})
				}
			}).catch( function (error){
				cb({error: 'Error en el servidor', serverError: error})
			})
		},
		upsert: function (propiedad, cb){
			axios.post(API + model + 'upsert', propiedad).then( function (res){
				if(!res.data.error){
					// Si viene bien devuelve la respuesta sin capsula: "response"
					cb(res.data.response)
				} else {
					// Caso contrario enviamos error
					cb({error: res.data.error})
				}
			}).catch( function (error){
				cb({error: 'Error en el servidor', serverError: error})
			})
		}
	}
}