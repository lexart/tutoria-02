const HomeComp = Vue.component('home-component', function (callback) {
	let viewUrl = './app/components/home/homeView.html'

	axios.get(viewUrl).then( function (result){
		let view = result.data;

		callback({
			data: function () {
				return {
					propiedades: []
				}
			},
			methods: {
				
			},
			mounted: function (){

				// Utilizando el servicio Propiedades
				Propiedades().all( (res)=> {
					if(!res.error){
						this.propiedades = chunk(res, 3);

						console.log("this.propiedades: ", this.propiedades)
					} else {
						console.log("error ::", res.error)
					}
				})
			},
			template: view
		})
	})
})