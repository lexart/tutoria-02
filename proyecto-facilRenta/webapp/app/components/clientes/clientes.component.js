const ClientesComp = Vue.component('clientes-component', function (callback) {
	let viewUrl = './app/components/clientes/clientesView.html'

	axios.get(viewUrl).then( function (result){
		let view = result.data;

		callback({
			data: function () {
				return {
					clientes: []
				}
			},
			methods: {
				
			},
			mounted: function (){
				let token = localStorage.getItem('token-app-' + APP_NAME);

				// Verifico el token
				verifyToken(token)

				const headers = {
					token: token
				}

				axios.get(API + 'clientes/all', {headers: headers}).then( (res) => {
					let clientes = res.data.response;
					this.clientes = clientes;
				})

				console.log("My Token :: ", token);
			},
			template: view
		})
	})
})