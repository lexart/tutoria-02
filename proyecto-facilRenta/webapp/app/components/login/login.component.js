const LoginComp = Vue.component('login-component', function (callback) {
	let viewUrl = './app/components/login/loginView.html'

	axios.get(viewUrl).then( function (result){
		let view = result.data;

		callback({
			data: function () {
				return {
					usr: {},
					error: '',
					success: ''
				}
			},
			methods: {
				loginUser: function () {
					const user = copy(this.usr);
					
					this.error = ''
					this.success = ''

					Usuarios().login(user, (res) => {
						if(!res.error){
							this.success = 'Usuario logueado correctamente';

							// Guardamos el usuario en el localStorage
							// Para verificar que esta logueado
							// TO-DO: Luego de implementar el JWT esto tiene que persistir un token
							
							window.localStorage.setItem('_user' + APP_NAME, JSON.stringify(res));

							setTimeout( function () {
								router.push('/')
							}, 500)
							
						} else {
							this.error = res.error;
						}
					})
				}
			},
			mounted: function (){

			},
			template: view
		})
	})
})