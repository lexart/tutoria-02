// Definir los componentes de forma dinámica
const Login   	= LoginComp;
const Clientes 	= ClientesComp;
const Cliente 	= ClienteComp;
const Dashboard = DashboardComp;
const Home      = HomeComp;
const Registro  = RegistroComp;
const AdmProp 	= AdmPropiedadesComp;

const routes = [
  { path: '/', component: Home },
  { path: '/login', component: Login },
  { path: '/dashboard', component: Dashboard },
  { path: '/clientes', component: Clientes },
  { path: '/cliente/edit/:id', component: Cliente },
  { path: '/cliente/new', component: Cliente },
  { path: '/register', component: Registro },
  { path: '/admin-propiedades', component: AdmProp }
]

const router = new VueRouter({
  routes 
})