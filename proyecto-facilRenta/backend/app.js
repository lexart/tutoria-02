const express 	 = require('express');
const bodyParser = require('body-parser');
const cors 		 = require('cors');
global.jwt		 = require('jsonwebtoken');
const app 	  	 = express();

// Requerir ENV
global.ENV 		 = require('./config/env')

// Requerir conexión a la base de datos
global.conn 	 = require('./config/conn');

// Requerir las rutas
const usersRoutes 		= require('./routes/users.routes')
const propiedadesRoutes = require('./routes/propiedades.routes')



app.use(cors())
app.use(bodyParser.json())

app.get('/', function (req, res) {
	let response = {hola: "Mundo"}

	res.set(['Content-Type', 'application/json']);
    res.send(response);
})

// Incluir la ruta de usuarios
app.use('/usuarios', usersRoutes)
app.use('/propiedades', propiedadesRoutes)



app.listen(ENV.port, function () {
  console.log(`FACIL RENTA :: ${ENV.port}`)
})