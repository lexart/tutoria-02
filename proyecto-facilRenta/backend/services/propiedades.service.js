const tablaNombre   = 'propiedades';
const Propiedades = {
	all: async function (offset, limit){

		offset = offset ? offset : 0;
		limit  = limit ? limit : 9;

		const sql = `
			SELECT ${tablaNombre}.*, 
				zonas.zona_nombre, 
				ciudades.ciudad_nombre,
				departamentos.depar_nombre AS departamento_nombre,
				paises.pais_nombre,
				rela_user_prop.rup_precio AS propiedad_precio
			FROM ${tablaNombre}
			
			INNER JOIN zonas         ON ${tablaNombre}.propiedad_zona = zonas.zona_id
			INNER JOIN ciudades      ON zonas.zona_ciudad = ciudades.ciudad_id
			INNER JOIN departamentos ON ciudades.ciudad_departamento = departamentos.depar_id
			INNER JOIN paises 		 ON departamentos.depar_pais = paises.pais_id

			INNER JOIN rela_user_prop ON rela_user_prop.rup_id_prop = ${tablaNombre}.propiedad_id

			WHERE ${tablaNombre}.propiedad_activo = ? AND rela_user_prop.rup_precio > 0
			LIMIT ${offset}, ${limit}
		`
		let response = []
		
		try {
			response = await conn.query(sql, [1]);
		} catch(e){}

		console.log("response: ", response);

		return response.length > 0 ? {response: response} : {error: 'No existe propiedades.'};
	},
	byUser: async function (id, offset, limit){

		offset = offset ? offset : 0;
		limit  = limit ? limit : 9;

		const sql = `
			SELECT ${tablaNombre}.*, 
				zonas.zona_nombre, 
				ciudades.ciudad_nombre,
				departamentos.depar_nombre AS departamento_nombre,
				paises.pais_nombre,
				rela_user_prop.rup_precio AS propiedad_precio,
				rela_user_prop.rup_duracion AS duracion_meses,
				rela_user_prop.rup_id
			FROM ${tablaNombre}
			
			INNER JOIN zonas         ON ${tablaNombre}.propiedad_zona = zonas.zona_id
			INNER JOIN ciudades      ON zonas.zona_ciudad = ciudades.ciudad_id
			INNER JOIN departamentos ON ciudades.ciudad_departamento = departamentos.depar_id
			INNER JOIN paises 		 ON departamentos.depar_pais = paises.pais_id

			INNER JOIN rela_user_prop ON rela_user_prop.rup_id_prop = ${tablaNombre}.propiedad_id

			WHERE ${tablaNombre}.propiedad_activo = ? AND rela_user_prop.rup_id_user = ?
			LIMIT ${offset}, ${limit}
		`
		let response = []
		
		try {
			response = await conn.query(sql, [1, id]);
		} catch(e){}

		return response.length > 0 ? {response: response} : {error: 'No existe propiedades.'};
	},
	upsert: async function (propiedad) {

		if(!propiedad.propiedad_id){
			// Insertar en la base
			const sql = `
				INSERT INTO ${tablaNombre}
				(propiedad_foto, propiedad_zona, propiedad_descripcion, propiedad_precio)
				VALUES
				(?, ?, ?, ?)
			`

			let arrInsert = [propiedad.propiedad_foto, propiedad.propiedad_zona, propiedad.propiedad_descripcion, 0]
			let response = {
				insertId: 0
			}
			
			try {
				response = await conn.query(sql, arrInsert);

				// Verificamos que la propiedad se inserto correctamente
				if(response.insertId > 0){

					// Insertar dentro de la tabla de relacion
					// Accion: 1 por defecto es "propietario"
					// Accion: 2 alquilado
					const sqlRel = `
						INSERT INTO rela_user_prop
						(rup_precio, rup_moneda, rup_id_user, rup_id_prop, rup_duracion, rup_accion, rup_calificacion)
						VALUES
						(?, ?, ?, ?, ?, ?, ?)
					`
					let arrInsertRel = [propiedad.propiedad_precio, "$", propiedad.user_id, response.insertId, propiedad.duracion_meses, 1, 0]

					response = await conn.query(sqlRel, arrInsertRel);

				} else {
					return {error: 'Error al insertar propiedad (antes de la relacion)'}
				}

			} catch(e){}

			return response.insertId > 0 ? {response: 'Propiedad ingresada correctamente.'} : {error: 'Error al insertar la propiedad.'};
		} else {
			// Actualizo
			const sql = `
				UPDATE ${tablaNombre} SET 
					propiedad_foto = ?, 
					propiedad_zona = ?, 
					propiedad_descripcion = ?
				WHERE propiedad_id = ?
			`

			let arrInsert = [propiedad.propiedad_foto, propiedad.propiedad_zona, propiedad.propiedad_descripcion, propiedad.propiedad_id]
			let response = {
				affectedRows: 0
			}

			try {
				response = await conn.query(sql, arrInsert);

				console.log("response - edit:", response)

				// Verificamos que la propiedad se inserto correctamente
				if(response.affectedRows > 0){

					// Insertar dentro de la tabla de relacion
					// Accion: 1 por defecto es "propietario"
					// Accion: 2 alquilado
					const sqlRel = `
						UPDATE rela_user_prop SET
							rup_precio = ?,
							rup_duracion = ?
						WHERE rup_id = ?
					`
					let arrInsertRel = [propiedad.propiedad_precio, propiedad.duracion_meses, propiedad.rup_id]

					response = await conn.query(sqlRel, arrInsertRel);

					console.log("response - edit:", response)

				} else {
					return {error: 'Error al actualizar propiedad (antes de la relacion)'}
				}

			} catch(e){}

			return response.affectedRows > 0 ? {response: 'Propiedad actualizada correctamente.'} : {error: 'Error al actualizar la propiedad.'};
		}
	}
}
module.exports = Propiedades;