const tablaNombre   = 'usuarios';
const md5 			= require('md5')

const Usuarios = {
	registro: async function (user) {
		// 
		const sql = `
			INSERT INTO ${tablaNombre} 
				(user_nombre, user_apellido, user_email, user_pass, user_tipo)
			VALUES
				(?, ?, ?, ?, ?)
		`
		let arrInsert = [user.user_nombre, user.user_apellido, user.user_email, md5(user.user_pass), 1]

		let response = {
			insertId: 0
		}
		
		try {
			response = await conn.query(sql, arrInsert);

		} catch(e){}

		return response.insertId > 0 ? {response: 'Usuario registrado correctamente.'} : {error: 'Error al registrar usuario.'};
	},
	login: async function (user) {
		const sql = `
			SELECT user_nombre, user_apellido, user_email, user_id FROM ${tablaNombre}
			WHERE ${tablaNombre}.user_email = ? AND ${tablaNombre}.user_pass = ?
		`

		let response = []
		
		try {
			response = await conn.query(sql, [user.user_email, md5(user.user_pass)]);
		} catch(e){}

		console.log("response: ", response);

		return response.length > 0 ? {response: response[0]} : {error: 'Login incorrecto.'};
	}
}
module.exports = Usuarios;