const mysql      = require('mysql');
const connection = mysql.createConnection(ENV.configDatabase);
 
connection.connect();

const Conn = {
	query: function (sql, arr) {
		return new Promise( (resolve, reject) => {
			connection.query(sql, arr, function (error, results, fields) {			  
			  if (error){
			  	resolve(error)
			  } else {
			  	resolve(results);
			  }

			});
		})
	}
}

module.exports = Conn;