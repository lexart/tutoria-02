const express = require('express');
const router = express.Router();

// Llamo al servicio propiedades
const Propiedades = require('../services/propiedades.service');

// Todos los usuarios
router.get('/destacadas', async function (req, res){
	let response = await Propiedades.all(0,9)

	res.set(['Content-Type', 'application/json']);
    res.send(response);
})

router.get('/by-user/:id', async function (req, res){
	let response = await Propiedades.byUser(req.params.id, 0,9)

	res.set(['Content-Type', 'application/json']);
    res.send(response);
})

router.post('/upsert', async function (req, res){
	let response = await Propiedades.upsert(req.body)

	res.set(['Content-Type', 'application/json']);
    res.send(response);
})

module.exports = router;