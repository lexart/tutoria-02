const express = require('express');
const router = express.Router();
const Users  = require('../services/users.service')


// Todos los usuarios
router.post('/registro', async function (req, res){
	let response = await Users.registro(req.body)

	res.set(['Content-Type', 'application/json']);
    res.send(response);
})

// Todos los usuarios
router.post('/login', async function (req, res){
	let response = await Users.login(req.body)

	res.set(['Content-Type', 'application/json']);
    res.send(response);
})

module.exports = router;