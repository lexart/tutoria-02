# Fácil Renta v1.0.0

## RoadMap

- Base de datos [ ]
	- Tablas: usuarios, propiedades [x]
		- CRUD
			- usuarios: (id [PK], nombre, email [UNIQUE], tipo, activo, fechaCreado, fechaEditado)
				- Observación: El usuario en principio puede ser cliente ó propietario
			
			- propiedades: (id [PK], foto[url], descripcion, pais, dpto, ciudad, zona, activo, tipo, activo, fechaCreado, fechaEditado )

			- tarjetas: (id [PK], nombre, moneda, banco, pais, activo, fechaCreado, fechaEditado)

		- RELACIONES: 
			- Relación: usuario - propiedad 1:N
			- Accion: [alquilado, visita, reserva, propietario]
				- usuario_propiedad (id [PK], idUsuario [FK], idPropiedad [FK], accion, duracion, activo, fechaCreado, fechaEditado)

			- Relación: usuario - tarjeta 1:N
				- usuario_tarjeta (id [PK], idUsuario [FK], idTarjeta [FK], activo, fechaCreado, fechaEditado)

			- Relación: usuario - propiedad 1:N
				- calificacion - (id [PK], idUsuario [FK], idPropiedad [FK], calificacion, activo, fechaCreado, fechaEditado)

	- Script de tablas [x]
		- Script .sql

- Backend [ ]
	- API's
		- ABM Tarjetas [ ]
			- GET: Listado de tarjetas
		- ABM usuarios [x]
			- POST: Registro de usuario [x]
			- POST: Login [x]
		
		- ABM propiedades [] Obs: Solo para usuarios tipo: propietario
			- POST: Registro de propiedad [x]
				
				- Crea la propiedad en la tabla propiedades [x]
				- Crea la relación en usuario_propiedad [x]

			- GET: Obtener propiedades con: (tarjetas disp, precio, etc) [ ]
			- GET: Buscar propiedades [ ]
					- Pais
					- Dpto 
					- Ciudad
					- Zona
			
			- POST: Solicitar visita [ ]
			- POST: Solicitar reserva [ ] 
			- POST: Alquilar ahora [ ]
			- GET: Obtener propiedad por ID

		- Integraciones - MercadoPago Sandbox [ ]
			- POST: Alquilar ahora

		- Autenticación: JWT middleware [ ]

- WebApp [ ]
	
	- Home [ ]
		- Home con botones Ingresar / Registrar en header [x]
		- Buscador de propiedades (estático) [ ]
		- Listado de propiedades "destacadas" (no más de 9) [x]

	- Flujo registro [x]
		
		- Registrar: Formulario de registro [x]
		- Registro completo: Página de "success" ToDo
		- Crea la relación en usuario_tarjeta

	- Flujo login [ ]

		- login: Formulario de ingreso [x]
		- Si es sucess redireccionar a la HOME [x]
		- Habilitar: Alquilar ahora, solicitar visita, reservar en el listado de propiedades [ ]

	- Flujo propiedades [ ]

		- Buscador home: Puede buscar propiedades por spec. [ ]
			- Si es sucess redirecciona a listado de propiedades [ ]
			- Puede seleccionar una propiedad para ver detalles
		
		- Detalle de propiedad [ ]
			- Acciones [ ]
				- Solicitar visita [ ]
				- Solicitar reserva [ ]
				- Alquilar ahora [ ]









